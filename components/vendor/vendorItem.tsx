import React, { useEffect, useRef, useState } from "react";
import style from "./vendorItem.module.scss";
import Image from "next/image";

const VendorItem = (vendor: any) => {
  const pageEnd: any = useRef();
  let num = 1;
  useEffect(() => {
    if (vendor.loading) {
      const observer = new IntersectionObserver(entries => {
        if (entries[0].isIntersecting) {
          num++;
          vendor.loadMore();
          if (num >= 10) {
            observer.unobserve(pageEnd.current)
            vendor.handleLoading()

          }
        }

      }, { threshold: 1 });
      
      observer.observe(pageEnd.current)
    }
  }, [vendor.loading, num])

  return (
    <React.Fragment>
      <a key={vendor.id} className={style.vendorCard} href="#">
        <header className={style.vendorCard_header}>
          <div className={style.vendorCard_cover}>
            <img className={style.vendorCard_img} src={vendor.backgroundImage} alt="name" />
          </div>
          <div className={style.vendorCard_logo}>
            <img src={vendor.defLogo} alt="پیتزا پرپروک (جردن)" className="_3_9MC _3ocXi" />
          </div>
        </header>
        <div className={style.vendorCard_body}>
          <div className={style.vendorCard_body__mainTitle}>
            <div className={style.vendorCard_body__title}><h3 >{vendor.title}</h3></div>
            <div className={style.vendorCard_body__rate}><span><span className={style.vendorCard_body__rateTotal}>(</span>{vendor.voteCount}<span className={style.vendorCard_body__rateTotal}>)</span></span>&nbsp;<div className={style.vendorCard_body__rateMain}>
              <span >{vendor.rate}</span>&nbsp;
              <Image src="/images/star.svg" alt="rate" width={12} height={12} />

            </div>
            </div>
          </div>
          <div className={style.vendorCard_body__mainDesc}>
            <div className={style.vendorCard_body__desc}>
              {vendor.description}

            </div>
          </div>
          <div className={style.vendorCard_body__delivery}><span className={style.vendorCard_body__deliveryType}>{vendor.deliveryGuarantee ? "ارسال اکسپرس" : "پیک فروشنده "}</span><span>{vendor.deliveryFee}</span> <span>تومان</span></div>
        </div>
      </a>
      <span ref={pageEnd}></span>
    </React.Fragment>
  )
}
export default VendorItem