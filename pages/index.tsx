import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useDispatch, useSelector } from "react-redux";
import VendorItem from '../components/vendor/vendorItem';
import { fetchVendors } from './../services/vendorService';
import { saveVendors } from '../redux/vendor/vendor.action';
import { useState } from 'react';
import { Vendor} from '../redux/vendor/vendor.types';
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { useEffect } from 'react';
import { useCallback } from 'react';
import Image from "next/image";

const Home: NextPage = () => {
  const dispatch: ThunkDispatch<any, any, AnyAction> = useDispatch();
  const [loading, setLoading] = useState(false)
  const [loadingVendors, setLoadingVendors] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const [perPage, setPerPage] = useState(10);

  let vendors = useSelector((state: any) => state.vendor.vendors.filter((v: { type: string }) => v.type == "VENDOR"));
  const getVendors = useCallback((pageNumber: number) => {
    fetchVendors(pageNumber, perPage).then((data) => {
      dispatch(saveVendors([...vendors, ...data.vendors]))
      setLoadingVendors(data.loadingVendors)
    })
  }, [pageNumber],
  );

  const handleScroll = (e:{}|any) => {
    const scrollHeight = e.target.documentElement.scrollHeight;
    const currentHeight = Math.ceil(
      e.target.documentElement.scrollTop + window.innerHeight
    );
    if (currentHeight + 1 >= scrollHeight) {
      setLoading(true)

    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);
  useEffect(() => {
    getVendors(pageNumber)
  }, [pageNumber])

  function loadMore(num: number) {
    setPageNumber(pageNumber + num);
    setLoading(false)

  }

  return (
    <div className={styles.container}>
      <Head>
        <title>SnappFood</title>
        <meta name="description" content="SnappFood Task" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main} data-testid="result">
        {/* VENDOR LIST */}
        {
          !loadingVendors ?
            vendors && vendors.map((vendor:Vendor) => (

              <VendorItem key={vendor.data.id} {...vendor.data} loadMore={() => loadMore(1)} loading={loading} handleLoading={() => setLoading(false)} data-testid="list"/>
            )) :
            <Image width="300" height="300" src="/images/Ghost.gif" alt="loading" />
        }
      </main>
    </div>
  )
}

export default Home
