import API_URL from "../configurations/environment";
import axios from "axios";

export const fetchVendors = async (pageNumber,perPage) => {
    let vendors = [];
    let totalCount = 0;
    let loadingVendors = true
    try {
        await axios({
            method: "get",
            url: `${API_URL}/vendors-list?page=${pageNumber}&page_size=${perPage}&lat=35.754&long=51.328`,

        }).then((response) => {
            const data = response.data.data.finalResult;
            vendors = data;
            totalCount = response.data.data.count;
            loadingVendors = false;

        })
    } catch (error) {
        console.log('error', error)
    }
  return {vendors,totalCount,loadingVendors};

}