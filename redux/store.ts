import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import rootReducer from "../redux/root-reducer";

const middlewares = [logger];

export const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares, thunk)
);
export default store;
