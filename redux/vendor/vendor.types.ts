export interface Vendor {
  data:{
    id:number
  };
  id: number;
  vendorCode: string;
  title: string;
  description: string;
  rate: number;
  logo: string;
  voteCount: number;
  backgroundImage: string;
  defLogo: string;
  deliveryGuarantee: boolean;
  deliveryFee: number;
  loading: boolean;
  loadMore: ()=>any;
  handleLoading: ()=>any;


}
export enum VendorActionTypes {
  SAVE_VENDORS = "SAVE_VENDORS",
  FETCH_VENDOR_LOADING = "FETCH_VENDOR_LOADING",
}
export interface VendorState {
  readonly loading: boolean;
  readonly vendors: Vendor[];
}
