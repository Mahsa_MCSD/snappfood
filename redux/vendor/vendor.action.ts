import API_URL from "../../configurations/environment";
import {VendorActionTypes} from "./vendor.types"
import { ActionCreator, Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { ApplicationState } from './../root-reducer';
export type AppThunk = ActionCreator<
  ThunkAction<void, ApplicationState, null, Action<string>>
>;
export const saveVendors:AppThunk=(item)=>(dispatch:Dispatch)=>{
    dispatch({
        type:VendorActionTypes.SAVE_VENDORS,
        payload:item
    })
}