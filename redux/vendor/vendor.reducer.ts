import { Reducer } from "redux";
import { VendorActionTypes, VendorState } from './vendor.types';

export const INITIAL_STATE: VendorState = {
    vendors: [],   
    loading: false
  };

const vendorReducer:Reducer<VendorState> = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case VendorActionTypes.SAVE_VENDORS:
            return {
                ...state,
                vendors: action.payload
            }
        case VendorActionTypes.FETCH_VENDOR_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        default:
            return state;
    }
}
export default vendorReducer;