import { combineReducers } from "redux";
import vendorReducer from './vendor/vendor.reducer';
import { VendorState } from "./vendor/vendor.types";

export interface ApplicationState{
    vendor:VendorState,
}


const appReducer  = combineReducers({
    vendor:vendorReducer
  });
const rootReducer = (state:any, action:any) => {
 
  return appReducer(state, action);
  }
export default rootReducer;


