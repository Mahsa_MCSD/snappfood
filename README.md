
# Snappfood task

vendors’ list implemmentation.


## Installation

Install project with npm or yarn

```bash
  yarn yarn dev
  npm start npm run dev
```
    
## Running Tests

To run tests, run the following command

```bash
  npm run test
```


## Environment Variables

 you can find API_URL here in /configurations/environment

`API_URL` 




## Tech Stack

**Client:** React, Redux, Scss, Nextjs, Axios, Jest ,TypeScript, Thunk




## Usage/Examples
fetch vendors by vendor service in services/vendorService.js
```javascript
fetchVendors(pageNumber, perPage).then((data) => {            
...
})
```
In Redux used thunk as a middleware and handle async requests
redux/vendor/vendor.action vendor.reducer vendor.types
```
export const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares, thunk)
);
```
inserted combine reducer only for future development
```
const appReducer  = combineReducers({
    vendor:vendorReducer
  });
```
load more data pure infinite scroll 
handle scroll by e.target.documentElement and window.addEventListener
```javascript
const handleScroll = (e) => {
    const scrollHeight = e.target.documentElement.scrollHeight;
    const currentHeight = Math.ceil(
      e.target.documentElement.scrollTop + window.innerHeight
    );
    if (currentHeight + 1 >= scrollHeight) {
      setLoading(true)

    }
}
```
VendorItem component display preview of cards
```
import VendorItem from '../components/vendor/vendorItem'
```
Scss use mixin for shared Usage and some variables for easy access
```
@mixin font-style($size: var(--text-small),$weight:var(--font-regular),$color:var(--text-color)) {
    font-size: $size;
    font-weight: $weight;
    color: $color;
  }

  :root {
  --alpha-carbon-grey: rgba(64, 64, 64, 0.04);
  --color-less-dark:#363a3e;

}
  ```
  test with jest and mock axios
  ```
  jest.mock('axios');

  ```

## Feedback

If you have any feedback, please reach out to me at mahsaashrafi71@gmail.com

