import axios from 'axios';

import { fetchVendors } from './../services/vendorService';

jest.mock('axios');

describe('fetchVendors', () => {
  it('fetches successfully data from an API', async () => {
    const data = {
        loadingVendors:true,
        totalCount:0,
        vendors:[]
    };

    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    await expect(fetchVendors(1,10)).resolves.toEqual(data);
  });

  it('fetches erroneously data from an API', async () => {
    const errorMessage = 'Network Error';

    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(fetchVendors(1,10)).rejects.toThrow(errorMessage);
  });
});